from django.db import models
from django.core.validators import RegexValidator


class TypeOfEquipment(models.Model):
    keyoft = models.CharField('Код', max_length=99, primary_key=True, validators=[
        RegexValidator(
            regex=r"^[A-Z0-9]{3}[A-Z]{2}[A-Z0-9][A-Z_@-][A-Z0-9][A-Za-z0-9]{2}"
        )])
    typename = models.CharField('Наименование типа', max_length=200)
    mask = models.TextField('Маска серийного номера')


class Equipment(models.Model):
    eqoftype = models.ForeignKey(TypeOfEquipment, on_delete=models.CASCADE, primary_key=True)
    keyofeq = models.CharField('КодОборудования', max_length=99)
    keytype = models.CharField('код типа', max_length=99)
    serialnum = models.CharField('Серийный номер', max_length=99)


