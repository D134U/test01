from django import forms
from django.forms import ModelForm, TextInput
from .models import TypeOfEquipment
from .models import Equipment


class TypeOfEquipmentForm(ModelForm):
    class Meta:
        model = TypeOfEquipment
        fields = ['typename', 'mask']

        widgets = {
            "typename": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Наименование типа'
            }),
            "mask": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Маска серийного номера'
            }),

        }




