from django.shortcuts import render
from .forms import TypeOfEquipmentForm
import re


# Create your views here.
def addtype(request):
    error = ''
    if request.method == 'POST':
        form = TypeOfEquipmentForm(request.POST)

        if form.is_valid():
            form.save()
        else:
            error = 'Что то пошло не так'
    form = TypeOfEquipmentForm

    data = {
        'form': form
    }
    return render(request, 'index.html', data)
